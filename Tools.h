//
// Created by johndoe on 3/27/20.
//
#include <string>

#ifndef ONE_TOOLS_H
#define ONE_TOOLS_H

using namespace std;
namespace skl {
	char *StringToCharPtr(string str) {
		char* cstr = new char[str.size() + 1]();

		std::copy(str.begin(), str.end(), cstr);
		cstr[str.size()] = '\0';

		return cstr;
	}
}

#endif //ONE_TOOLS_H
