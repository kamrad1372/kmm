//
// Created by johndoe on 3/25/20.
//
#include <memory>
#include <exception>
#include "AbstractAllocator.h"

using namespace std;

#ifndef ONE_LISTALLOCATOR_H
#define ONE_LISTALLOCATOR_H
namespace skl {
	class ListAllocator : public AbstractAllocator {
		private:
		unsigned long freeSpaceSA; //free address start address in created memory space

		bool canAllocateSize(unsigned long size) {
			return freeSpaceSA += size < startAddress + totalCapacity;
		}

		public:
		ListAllocator(unsigned long totalCapacity)
			: AbstractAllocator(totalCapacity) {
			freeSpaceSA = startAddress;

#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "List Allocator Created : %d", totalCapacity);
#endif
		}

		void *allocate(unsigned long size) override {
			if (isHaveFreeSpace() && canAllocateSize(size)) {
				void *ptr = reinterpret_cast<void *>(freeSpaceSA);
#if KLM_SELF_DEBUG_BUILD
				LOG_F(INFO, "Allocate In Pool %d In Address : %d", size, freeSpaceSA);
#endif
				freeSpaceSA += size;
				return ptr;
			} else {
				throw std::range_error("There is not free space in this pool");
			}
		}

		void freeSpace(unsigned long position) override {
			throw logic_error("You Must Reset Allocator Entirely");
		}

		bool isHaveFreeSpace() override {
			return freeSpaceSA-1 < (startAddress + totalCapacity);
		}

		void reset() override {
			freeSpaceSA = startAddress;
		}

		template<typename T, typename... _Args>
		std::shared_ptr<T> getSharedPtr(_Args &&... args) {
			void *ptr = allocate(sizeof(T));
#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "Pool Shared Pointer Created At : %d", reinterpret_cast<unsigned long>(ptr));
#endif
			return std::shared_ptr<T>(new(ptr) T(std::forward<_Args>(args)...));
		}
	};
}

#endif //ONE_LISTALLOCATOR_H
