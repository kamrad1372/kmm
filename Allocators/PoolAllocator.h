//
// Created by johndoe on 3/19/20.
//

#include <cstdlib>
#include "AbstractAllocator.h"
#include <exception>
#include <stdexcept>
#include <queue>
#include <utility>
#include <memory>

#if KLM_SELF_DEBUG_BUILD
#include <loguru.hpp>
#endif

#ifndef ONE_POOLALLOCATOR_H
#define ONE_POOLALLOCATOR_H
namespace skl {
	class PoolAllocator : public AbstractAllocator {
		private:
		size_t chunkSize;
		long chunkCount;
		std::queue<size_t> freeChunks;

		template<typename T>
		void smartPtrDeleter(T *ptr) {
#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "Pool Smart Pointer Delete At : %d", reinterpret_cast<size_t>(ptr));
#endif
			freeSpace(reinterpret_cast<size_t>(ptr));
		}

		public:
		PoolAllocator(size_t totalCapacity, size_t chunkSize)
			: AbstractAllocator(totalCapacity) {

			freeChunks = std::queue<size_t>();
			this->chunkSize = chunkSize;
			chunkCount = totalCapacity / chunkSize;

			reset();
#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "Pool Allocator Created : %d", totalCapacity);
#endif
		}

		void *allocate(size_t size = 0) override {
			if (isHaveFreeSpace()) {
				freeChunks.pop();
#if KLM_SELF_DEBUG_BUILD
				LOG_F(INFO, "Allocate In Pool %d In Address : %d", chunkSize, startAddress + freeChunks.front());
#endif
				return reinterpret_cast<void *>(startAddress + freeChunks.front());
			} else {
				throw std::range_error("There is not free space in this pool");
			}
		}

		void freeSpace(size_t position) override {
			freeChunks.push(position);
#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "Pool Chunk Space Freed At : %d", position);
#endif
		}

		bool isHaveFreeSpace() override {
			return freeChunks.size() != 0;
		}

		void reset() override {
			for (size_t i = 0; i < chunkCount; i++) {
				freeChunks.push(i * chunkSize);
			}
		}

		template<typename T, typename... _Args>
		std::shared_ptr<T> getSharedPtr(_Args &&... args) {
			void *ptr = allocate();
#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "Pool Shared Pointer Created At : %d", reinterpret_cast<unsigned long>(ptr));
#endif
			return std::shared_ptr<T>(new (ptr) T(std::forward<_Args>(args)...), [&](T* ptr){
				this->smartPtrDeleter<T>(ptr);
			});
		}
	};
} // namespace skl
#endif //ONE_POOLALLOCATOR_H
