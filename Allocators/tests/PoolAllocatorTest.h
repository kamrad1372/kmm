//
// Created by johndoe on 3/20/20.
//
// #include "../PoolAllocator.h"
#include <chrono>
#include <iostream>
#include <cstdlib>
#include <loguru.hpp>
#include <bits/basic_string.h>
#include <list>
#include "../PoolAllocator.h"
#include "../FreeListAllocator.h"
#include <memory>

#ifndef ONE_POOLALLOCATORTEST_H
#define ONE_POOLALLOCATORTEST_H
class Test {
	public:
	unsigned long a = 1;
	unsigned long b = 2;
};

namespace skl {

	void testAllocatePoolAllocator(unsigned long poolCap, unsigned long chunkSize, int count) {

		auto start = std::chrono::system_clock::now();
		PoolAllocator allocator = PoolAllocator(poolCap, chunkSize);
//		FreeListAllocator allocator = FreeListAllocator(poolCap);
		auto end = std::chrono::system_clock::now();
		std::cout << "create pool time is : " << (end - start).count() << std::endl;
		std::list<void *> ptrs;
		for (int i = 0; i < count; i++) {
			void *ptr = allocator.allocate();
			ptrs.push_front(ptr);
//		LOG_F(INFO, &std::to_string(reinterpret_cast<unsigned long> (ptr))[0]);
		}

		for (void *ptr : ptrs) {
			allocator.freeSpace(reinterpret_cast<unsigned long> (ptr));
		}

		list<std::shared_ptr<Test>> sharedPtrs;
		for (int i = 0; i < count; i++) {
			sharedPtrs.push_front(allocator.getSharedPtr<Test>());
		}

		for (int i = 0; i < count; i++) {
			sharedPtrs.pop_front();
		}

		end = std::chrono::system_clock::now();
		std::cout << "current test result is : " << (end - start).count() << std::endl;
	}

	template<void (*func)()>
	void callWithTimer() {
		//  void callWithTimer(void (MemoryManagerTest::*function)()  ){
		auto start = std::chrono::system_clock::now();
		(*func)();
		auto end = std::chrono::system_clock::now();
		std::cout << "current test result is : " << (end - start).count() << std::endl;
	}
}
#endif //ONE_POOLALLOCATORTEST_H
