//
// Created by johndoe on 3/25/20.
//
#include "../ListAllocator.h"
#include <chrono>
#include <iostream>
#include <cstdlib>
#include <cmath>

#ifndef ONE_LISTALLOCATORTEST_H
#define ONE_LISTALLOCATORTEST_H
namespace skl {
	void testAllocateListAllocator(unsigned long poolCap, unsigned long chunkSize, int count) {
		auto start = std::chrono::system_clock::now();
		ListAllocator allocator = ListAllocator(poolCap);
		auto end = std::chrono::system_clock::now();
		std::cout << "create pool time is : " << (end - start).count() << std::endl;
		for (int i = 0; i < count; i++) {
		unsigned long randomSize = rand() % chunkSize + 64;
			allocator.allocate(chunkSize);
		}
		end = std::chrono::system_clock::now();
		std::cout << "current test result is : " << (end - start).count() << std::endl;
		allocator.reset();
	}
}
#endif //ONE_LISTALLOCATORTEST_H
