//
// Created by johndoe on 3/20/20.
//
#include "../FreeListAllocator.h"
#include <chrono>
#include <iostream>
#include <cstdlib>
#include <cmath>

#ifndef ONE_FREELISTALLOCATORTEST_H
#define ONE_FREELISTALLOCATORTEST_H


namespace skl {

	void testAllocateFreeListAllocator(unsigned long poolCap, unsigned long chunkSize, int count) {
		auto start = std::chrono::system_clock::now();
		FreeListAllocator allocator = FreeListAllocator(poolCap);
		auto end = std::chrono::system_clock::now();
		std::cout << "create pool time is : " << (end - start).count() << std::endl;

		std::list<void *> ptrs;

		for (int i = 0; i < count; i++) {
			unsigned long randomSize = rand() % 128 + chunkSize;
			void *ptr = allocator.allocate(chunkSize);
			ptrs.push_front(ptr);

		}
		for (void *ptr : ptrs) {
			allocator.freeSpace(reinterpret_cast<unsigned long> (ptr));
		}

//		list<std::shared_ptr<Test>> sharedPtrs;
//		for (int i = 0; i < count; i++) {
//			sharedPtrs.push_front(allocator.getSharedPtr<Test>());
//		}
//
//		for (int i = 0; i < count; i++) {
//			sharedPtrs.pop_front();
//		}
		end = std::chrono::system_clock::now();
		std::cout << "current test result is : " << (end - start).count() << std::endl;
	}

}
#endif //ONE_FREELISTALLOCATORTEST_H
