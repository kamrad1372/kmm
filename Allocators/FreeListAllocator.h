//
// Created by johndoe on 3/20/20.
//

#include <cstdlib>
#include "AbstractAllocator.h"
#include <map>
#include <utility>
#include <exception>

#if KLM_SELF_DEBUG_BUILD

#include <loguru.hpp>
#include "Tools.h"

#endif

#ifndef ONE_FREELISTALLOCATOR_H
#define ONE_FREELISTALLOCATOR_H

using namespace std;

/**
 * @details : evey space have some flags int two place:
 * 		first flags : a "bool" valuse in first that show this space is filled or not
 * 					 privious flag`s address stored in next "unsigned long" flag
 * 					 and size of current flag in the next "unsigned long" flag
 * 		last flags : a "unsigned long" flag that store currnt space size
 * 					 and in next and last flag store the next flag`s addreess
 */
namespace skl {
	class FreeListAllocator : AbstractAllocator {
		private:
		map<unsigned long, unsigned long> spaces;
		unsigned long linkSize;

		unsigned long getLinkedSpaceSize(unsigned long size) {
			return size + sizeof(bool) + (3 * sizeof(unsigned long));
		}

		pair<unsigned long, unsigned long> getFreeSpaceFor(unsigned long size) {
			pair<unsigned long, unsigned long> targetSpace = pair<unsigned long, unsigned long>(startAddress,
																								totalCapacity);
			unsigned long linkedSize = getLinkedSpaceSize(size);

			if (spaces.size() == 1) {
				return targetSpace;
			} else if (spaces.size() == 0) {
				throw new range_error("There Is No Space In this Memory Section");
			} else {
				for (pair<unsigned long, unsigned long> freeSpace : spaces) {
					if (freeSpace.second >= linkedSize && freeSpace.second < targetSpace.second &&
						!getSpaceUsedFlag(freeSpace.first)) {
						targetSpace = freeSpace;
					}
				}
				if (targetSpace.second != totalCapacity) {
#if KLM_SELF_DEBUG_BUILD
					LOG_F(INFO, "Find Free Space At Address %s With Size : %s",
						  StringToCharPtr(to_string(targetSpace.first)),
						  StringToCharPtr(to_string(targetSpace.second)));
#endif
					return targetSpace;
				} else {
					throw new range_error("There Is No Space In this Memory Section");
				}
			}
		}

		void consumeSpace(pair<unsigned long, unsigned long> space, unsigned long targetSize) {
			if (getSpaceUsedFlag(space.first)) {
				throw bad_exception();
			} else {
				unsigned long linkedSize = getLinkedSpaceSize(targetSize);

				if (linkedSize > space.second) {
					throw new range_error("Out Of Bounds");
				} else if (linkedSize == space.second) {
					setSpaceUsedFlag(space.first, true);
#if KLM_SELF_DEBUG_BUILD
					LOG_F(INFO, "New Space Created At Address %s With Size : %s",
						  StringToCharPtr(to_string(space.first)), StringToCharPtr(to_string(space.second)));
#endif
				} else {
					pair<unsigned long, unsigned long> newSpace = pair<unsigned long, unsigned long>(space.first,
																									 linkedSize);
					unsigned long leftSpaceSddress = getLeftSpaceAddress(space.first);
					pair<unsigned long, unsigned long> newFreeSpace = pair<unsigned long, unsigned long>(
						space.first + linkedSize,
						space.second - linkedSize);

					spaces.erase(space.first);
					spaces.insert(newSpace);
					spaces.insert(newFreeSpace);

					setSpaceFlags(newSpace.first,
								  leftSpaceSddress == startAddress ? startAddress : leftSpaceSddress,
								  newSpace.first + newSpace.second,
								  true, newSpace.second);

					setSpaceFlags(newFreeSpace.first,
								  newSpace.first,
								  space.first + space.second,
								  false, space.second - newSpace.second);

#if KLM_SELF_DEBUG_BUILD
					LOG_F(INFO, "New Space Create At %s With Size %s And New Free Space Created At %s With Size %s",
						  StringToCharPtr(to_string(newSpace.first)), StringToCharPtr(to_string(newSpace.second)),
						  StringToCharPtr(to_string(newFreeSpace.first)),
						  StringToCharPtr(to_string(newFreeSpace.second)));
#endif
				}
			}
		}

		void releaseUsedSpace(unsigned long address) {
			unsigned long leftSpace = getLeftSpaceAddress(address);
			unsigned long rightSpace = getRightSpaceAddress(address);
			unsigned long spaceSize = getSpaceSize(address);

			if (address != startAddress &&
				address + spaceSize != startAddress + totalCapacity &&
				!getSpaceUsedFlag(leftSpace) &&
				!getSpaceUsedFlag(rightSpace)) {

				unsigned long size = getSpaceSize(leftSpace) + spaceSize + getSpaceSize(rightSpace);

				spaces.erase(leftSpace);
				spaces.erase(address);
				spaces.erase(rightSpace);

				spaces.insert(make_pair(leftSpace, size));
				setSpaceFlags(leftSpace,
							  getLeftSpaceAddress(leftSpace),
							  getLeftSpaceAddress(leftSpace) + size,
							  false, size);

#if KLM_SELF_DEBUG_BUILD
				LOG_F(INFO, "Space Merged From Two Sides And Free Space Created At %s With Size : %s",
					  StringToCharPtr(to_string(leftSpace)), StringToCharPtr(to_string(size)));
#endif

			} else if (address != startAddress && !getSpaceUsedFlag(leftSpace)) {
				unsigned long size = getSpaceSize(leftSpace) + spaceSize;
				spaces.erase(leftSpace);
				spaces.erase(address);
				spaces.insert(make_pair(leftSpace, size));
				setSpaceFlags(leftSpace,
							  getLeftSpaceAddress(leftSpace),
							  getLeftSpaceAddress(leftSpace) + size,
							  false, size);

#if KLM_SELF_DEBUG_BUILD
				LOG_F(INFO, "Space Merged From Left Side And Free Space Created At %s With Size : %s",
					  StringToCharPtr(to_string(leftSpace)), StringToCharPtr(to_string(size)));
#endif

			} else if (address + spaceSize != startAddress + totalCapacity && !getSpaceUsedFlag(rightSpace)) {
				unsigned long size = spaceSize + getSpaceSize(rightSpace);
				spaces.erase(address);
				spaces.erase(rightSpace);
				spaces.insert(make_pair(address, size));
				setSpaceFlags(address,
							  getLeftSpaceAddress(address),
							  getLeftSpaceAddress(address) + size,
							  false, size);
#if KLM_SELF_DEBUG_BUILD
				LOG_F(INFO, "Space Merged From Right Side And Free Space Created At %s With Size : %s",
					  StringToCharPtr(to_string(address)), StringToCharPtr(to_string(size)));
#endif
			} else {
				setSpaceUsedFlag(address, false);
#if KLM_SELF_DEBUG_BUILD
				LOG_F(INFO, "Free Space Created At %s With Siz : %s", StringToCharPtr(to_string(leftSpace)),
					  StringToCharPtr(to_string(getSpaceSize(address))));
#endif
			}
		}

		/**
		 * @attention : is pointer address consume memory and where?
		 *
		 * @param address : address of target space
		 * @param preAddress : previous space address
		 * @param nextAddress : nest space address
		 * @param isUsed : is target space filled
		 * @param size : size of target space
		 */
		void setSpaceFlags(unsigned long address, unsigned long lAddress, unsigned long rAddress, bool isUsed,
						   unsigned long size) {
			setSpaceUsedFlag(address, isUsed);
			setSpaceSize(address, size);
			setLeftSpaceAddress(address, lAddress);
			setRightSpaceAddress(address, rAddress);
		}

		/**
		 * @brief : get is used flag of space address
		 * @param address of target space
		 * @return address of pointer to flag
		 */
		bool getSpaceUsedFlag(unsigned long address) {
			bool *value = (bool *) address;
			return *value;
		}

		void setSpaceUsedFlag(unsigned long address, bool state) {
			bool *value = (bool *) address;
			*value = state;
		}

		/**
		 * @brief : get left previous space from first flags
		 * @param : address : current space address
		 */
		unsigned long getLeftSpaceAddress(unsigned long address) {

			unsigned long *value = (unsigned long *) (address + sizeof(bool));
			return *value;
		}

		void setLeftSpaceAddress(unsigned long address, unsigned long lAddress) {
			unsigned long *value = (unsigned long *) (address + sizeof(bool));
			*value = lAddress;
		}

		/**
		 * @brief : get right next space from last flags
		 * @param : address : current space address
		 */
		unsigned long getRightSpaceAddress(unsigned long address) {
			unsigned long spaceSize = getSpaceSize(address);
			unsigned long *value = (unsigned long *) (address + spaceSize - sizeof(unsigned long));
			return *value;
		}

		void setRightSpaceAddress(unsigned long address, unsigned long rAddress) {
			unsigned long spaceSize = getSpaceSize(address);
			unsigned long *value = reinterpret_cast<unsigned long *>(address + spaceSize - sizeof(unsigned long));
			*value = rAddress;
		}

		/**
		 * @brief : get current space size from first flags
		 * @param address : address of this space
		 * @return : size of current address
		 */
		unsigned long getSpaceSize(unsigned long address) {
			unsigned long *value = (unsigned long *) (address + sizeof(bool) + sizeof(unsigned long));
			return *value;
		}

		void setSpaceSize(unsigned long address, unsigned long size) {
			unsigned long *value = (unsigned long *) (address + sizeof(bool) + sizeof(unsigned long));
			*value = size;
		}

		/**
		 * @brief get main pointer address of space if filled
		 * @param address of current space
		 * @return address of object in memory
		 */
		unsigned long getSpaceValueAddress(unsigned long address) {
			unsigned long value = reinterpret_cast<unsigned long> (address + sizeof(bool) +
																   (2 * sizeof(unsigned long)));
			return value;
		}

		void initFirstSpace() {
			spaces.insert(make_pair(startAddress, totalCapacity));

			setSpaceUsedFlag(startAddress, false);
			setSpaceSize(startAddress, totalCapacity);
			setRightSpaceAddress(startAddress, startAddress + totalCapacity);
			setLeftSpaceAddress(startAddress, startAddress);

			*(bool *) (startAddress + sizeof(bool)) = true;
			*(unsigned long *) (startAddress + sizeof(bool) +
								sizeof(unsigned long)) = startAddress;
			*(unsigned long *) (startAddress + sizeof(bool) + (sizeof(unsigned long) * 2)) = totalCapacity;

			*(unsigned long *) (startAddress + totalCapacity - sizeof(unsigned long)) = startAddress + totalCapacity;
			*(unsigned long *) (startAddress + totalCapacity - (2 * sizeof(unsigned long))) = totalCapacity;
		}

		template<typename T>
		void smartPtrDeleter(T *ptr) {
#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "Pool Smart Pointer Delete At : %s",
				  StringToCharPtr(to_string(reinterpret_cast<unsigned long>(ptr))));
#endif
			freeSpace(reinterpret_cast<unsigned long>(ptr));
		}

		public:
		FreeListAllocator(unsigned long totalCapacity) : AbstractAllocator(totalCapacity) {
			linkSize = sizeof(unsigned long) * 2;
			initFirstSpace();
#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "Free List Allocator Created : %s", StringToCharPtr(to_string(totalCapacity)));
#endif
		}

		void *allocate(unsigned long size) override {

//			unsigned long targetSize = (linkSize * 2) + sizeof(bool) + size;
			pair<unsigned long, unsigned long> targetSpace = getFreeSpaceFor(size);
			consumeSpace(targetSpace, size);
#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "Allocate In Free List %s In Address : %s",
				  StringToCharPtr(to_string(getLinkedSpaceSize(size))), StringToCharPtr(to_string(targetSpace.first)));
			LOG_F(INFO, "Allocate Pointer Address is : %s",
				  StringToCharPtr(to_string(getSpaceValueAddress(targetSpace.first))));
#endif
			return reinterpret_cast<void *>(getSpaceValueAddress(targetSpace.first));
		}

		void freeSpace(unsigned long address) override {
			releaseUsedSpace(address - (sizeof(bool) + linkSize));
#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "Chunk Space Freed At : %s", StringToCharPtr(to_string(address)));
#endif
		}

		bool isHaveFreeSpace() override {
			try {
				getFreeSpaceFor(1);
				return true;
			} catch (range_error e) {
				return false;
			}
		}

		void reset() override {
			spaces.clear();
			initFirstSpace();
#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "Allocator Reset");
#endif
		}

		template<typename T, typename ... _Args>
		std::shared_ptr<T> getSharedPtr(_Args ... args) {
			unsigned long size = sizeof(T);
			void *ptr = allocate(size);
#if KLM_SELF_DEBUG_BUILD
			LOG_F(INFO, "Free List Shared Pointer Created At : %s",
				  StringToCharPtr(to_string(reinterpret_cast<unsigned long>(ptr))));
#endif
			return std::shared_ptr<T>(new(ptr) T(std::forward<_Args>(args)...), [&](T *ptr) {
				this->smartPtrDeleter<T>(ptr);
			});
		}

	};
}
#endif //ONE_FREELISTALLOCATOR_H
