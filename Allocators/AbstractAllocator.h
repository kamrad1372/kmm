//
// Created by johndoe on 3/19/20.
//

#ifndef ONE_ABSTRACTALLOCATOR_H
#define ONE_ABSTRACTALLOCATOR_H

#include <cstdlib>
#include <memory>
#include <exception>
#include <stdexcept>

class AbstractAllocator {
	private:
	void *allocateTotalCap(unsigned long totalCapacity) {
		return malloc(totalCapacity);
	}

	protected:
	unsigned long totalCapacity;
	void *startPointer;
	unsigned long startAddress;

	public:
	AbstractAllocator(unsigned long totalCapacity) {
		this->totalCapacity = totalCapacity;
		startPointer = allocateTotalCap(totalCapacity);
		startAddress = reinterpret_cast<unsigned long>(startPointer);
	}
	~AbstractAllocator(){
		free(startPointer);
	}

	virtual void *allocate(unsigned long size) {
		throw "Not implemented exception";
	}

	virtual void freeSpace(unsigned long position) {
		throw "Not implemented exception";
	}

	virtual bool isHaveFreeSpace() {
		throw "Not implemented exception";
	}

	virtual void reset() {
		throw "Not implemented exception";
	}
};


#endif //ONE_ABSTRACTALLOCATOR_H
